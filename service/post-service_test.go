package service

import (
	"testing"

	"gitlab.com/aryanicosa/golang-gin-gitlab/entity"
	"github.com/stretchr/testify/assert"
	
)

const (
	TITLE       = "Post Title"
	DESCRIPTION = "Post Description"
	URL         = "https://www.youtube.com/watch?v=ICwLGp49enk"
)

func getPost() entity.Post {
	return entity.Post{
		Title:       TITLE,
		Description: DESCRIPTION,
		URL:         URL,
	}
}

func TestFindAll(t *testing.T) {
	service := New()

	service.Save(getPost())

	posts := service.FindAll()

	firstPost := posts[0]
	assert.NotNil(t, posts)
	assert.Equal(t, TITLE, firstPost.Title)
	assert.Equal(t, DESCRIPTION, firstPost.Description)
	assert.Equal(t, URL, firstPost.URL)
}
